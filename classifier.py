import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import tqdm
from sklearn.metrics import classification_report
from torch.utils.data import DataLoader
from transformers import BertModel, BertTokenizer, AdamW, get_linear_schedule_with_warmup

import wandb
from dataset import VectorDataset
from memory import Memory


class ClassNet(nn.Module):
    def __init__(self, n_classes, model_name='bert-base-cased'):
        super(ClassNet, self).__init__()
        print("Making a neural network.")

        self.PRE_TRAINED_MODEL_NAME = model_name
        self.bert = BertModel.from_pretrained(self.PRE_TRAINED_MODEL_NAME)
        self.drop = nn.Dropout()
        self.out = nn.Linear(self.bert.config.hidden_size, n_classes)

    def forward(self, input_ids, attention_mask):
        _, pooled_out = self.bert(input_ids=input_ids, attention_mask=attention_mask, return_dict=False)
        output = self.drop(pooled_out)
        return self.out(output)


class Classifier:
    def __init__(self, batch_size=8, num_workers=1, n_classes=5, model_name='bert-base-cased', max_len=120):
        print("Making a classifier.")
        self.batch_size = batch_size
        self.num_workers = num_workers
        self.MAX_LEN = max_len

        self.train_loader = None
        self.test_loader = None
        self.val_loader = None
        self.optimizer = None
        self.scheduler = None
        self.loss_fn = None
        self.memory = Memory(nlp=None)

        if torch.cuda.is_available():
            self.device = torch.device("cuda")
            torch.cuda.empty_cache()
        else:
            self.device = torch.device("cpu")
        print(f"Using: {self.device}")

        self.net = ClassNet(n_classes).to(self.device)
        self.net.eval()
        print("Making a tokenizer.")
        self.tokenizer = BertTokenizer.from_pretrained(model_name)

    def fit_one(self, in_text: str, to_memory=True):
        """
        Predict the label for one string.
        :param in_text: input string
        :param to_memory: add to the memory
        :return: predicted label
        """
        self.net.eval()
        # encode
        encoding = self.tokenizer.encode_plus(
            in_text,
            add_special_tokens=True,
            return_token_type_ids=False,
            padding="max_length",
            max_length=self.MAX_LEN,
            return_attention_mask=True,
            return_tensors="pt"
        )

        in_id = encoding["input_ids"].to(self.device)
        mask = encoding['attention_mask'].to(self.device)

        out = self.net(in_id, mask)
        _, out = torch.max(out, dim=1)

        if to_memory:
            print(in_text, out)
            self.memory.append_(in_text, out)
        return out

    def train(self, train_df, test_df, val_df, epochs=1):
        """
        Train the BERT net.
        :param train_df: training dataframe
        :param test_df: testing dataframe
        :param val_df: validation dataframe
        :param epochs: how many training epochs
        """
        print("Preparing for NN training.")
        # make datasets and loaders
        ds_train = VectorDataset(train_df, max_len=self.MAX_LEN)
        ds_test = VectorDataset(test_df, max_len=self.MAX_LEN)
        ds_val = VectorDataset(val_df, max_len=self.MAX_LEN)

        self.train_loader = DataLoader(ds_train, batch_size=self.batch_size, num_workers=self.num_workers)
        self.test_loader = DataLoader(ds_test, batch_size=self.batch_size, num_workers=self.num_workers)
        self.val_loader = DataLoader(ds_val, batch_size=self.batch_size, num_workers=self.num_workers)

        # training prep
        self.optimizer = AdamW(self.net.parameters(), lr=2e-5, correct_bias=False)
        total_steps = len(self.train_loader) * epochs

        self.scheduler = get_linear_schedule_with_warmup(
            self.optimizer,
            num_warmup_steps=0,
            num_training_steps=total_steps
        )
        self.loss_fn = nn.CrossEntropyLoss().to(self.device)
        # print("Pre training report.")
        # self.report()

        wandb.init(project='VectorAI', entity='cichyt')
        wandb.watch(self.net)

        self._eval_model()
        self._train_model(epochs=epochs)
        self.report()

    def _train_model(self, epochs):
        """
        Training loop
        :param epochs: how many epochs
        :return: mean loss of the last epoch
        """
        print("Training...")
        self.net.train()
        for epoch in tqdm.tqdm(range(epochs)):
            losses = []
            n = 0
            correct_predictions = 0
            for d in tqdm.tqdm(self.train_loader):
                # preprocess
                input_ids = d[1].to(self.device)
                att_masks = d[2].to(self.device)
                targets = d[3].to(self.device)
                n += len(input_ids)

                # predict
                outs = self.net(input_ids, att_masks)
                _, preds = torch.max(outs, dim=1)
                loss = self.loss_fn(outs, targets)

                # metrics
                correct_predictions += torch.sum(preds == targets)
                losses.append(loss.item())

                # train
                loss.backward()
                nn.utils.clip_grad_norm_(self.net.parameters(), max_norm=1)
                self.optimizer.step()
                self.scheduler.step()
                self.optimizer.zero_grad()
                # log
                wandb.log({"train_loss": np.mean(losses),
                           "train_acc": correct_predictions / n})
            # save
            self.save_weights("checkpoint")

            self._eval_model()
        self.save_weights(f"epochs{epoch}")
        return np.mean(losses)

    def _eval_model(self):
        """
        Evaluate the model.
        :return: mean loss and accuracy
        """
        print("Evaluating...")
        self.net.eval()
        losses = []
        n = 0
        correct_predictions = 0
        with torch.no_grad():
            for d in tqdm.tqdm(self.val_loader):
                input_ids = d[1].to(self.device)
                att_masks = d[2].to(self.device)
                targets = d[3].to(self.device)
                n += len(input_ids)

                # predict
                outs = self.net(input_ids, att_masks)
                _, preds = torch.max(outs, dim=1)
                loss = self.loss_fn(outs, targets)

                correct_predictions += torch.sum(preds == targets)

                losses.append(loss.item())
        val_loss = np.mean(losses)
        val_acc = correct_predictions/n
        wandb.log({"val_loss": val_loss,
                   "val_acc": val_acc})
        return val_loss, val_acc

    def report(self):
        """
        Create the classification report on a test data.
        """
        print("Reporting...")
        self.net.eval()
        real_vals = []
        pred_vals = []
        pred_probs = []

        with torch.no_grad():
            for d in tqdm.tqdm(self.test_loader):
                input_ids = d[1].to(self.device)
                att_masks = d[2].to(self.device)
                targets = d[3].to(self.device)

                outs = self.net(input_ids, att_masks)
                _, preds = torch.max(outs, dim=1)

                real_vals.extend(targets)
                pred_probs.extend(outs)
                pred_vals.extend(preds)

        pred_vals = torch.stack(pred_vals).cpu()
        pred_probs = torch.stack(pred_probs).cpu()
        real_vals = torch.stack(real_vals).cpu()

        print(classification_report(real_vals, pred_vals))

    def save_weights(self, bonus_name=""):
        print("Saving model weights.")
        torch.save(self.net.state_dict(), f"./models/5class_bert_{bonus_name}.ph")
        if bonus_name != "checkpoint":
            wandb.save(f"./models/5class_bert_{bonus_name}.ph")

    def load_weights(self, bonus_name=""):
        self.net.load_state_dict(torch.load(f"./models/5class_bert_{bonus_name}.ph"))


if __name__ == "__main__":
    train_df = pd.read_csv("./data/train.csv")
    test_df = pd.read_csv("./data/test.csv")
    val_df = pd.read_csv("./data/val.csv")

    cls = Classifier()
    cls.train(train_df, test_df, val_df, epochs=1)
