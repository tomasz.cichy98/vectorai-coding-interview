# VectorAI coding interview
One package that I am using creates some warnings then imported. These warnings can be ignored.  The code takes the input lines from the txt file but can also be used to classify a line or a list of lines from other sources.

## Update (12/05/2021)
Link to the a subset of the dataset and sources in the second sheet: [here](https://docs.google.com/spreadsheets/d/1M3dJdsdR8ZWcPKU8_7A7SJ7suiytmnad2Dmg73ZoZPE/edit?usp=sharing).  
Runs: [here](https://wandb.ai/cichyt/VectorAI?workspace=user-cichyt).
1. Compile a dataset from various online resources,
1. Implement PyTorch Dataset and DataLoader,
1. Use BERT and a fully connected layer to make a classifier,
1. Train and evaluate the classifier,
1. Log using wandb

TODO:
1. ~~Add serial numbers to the dataset~~
1. ~~Use the network for predictions~~
1. Add augmentations and noise
1. Balance the classes
1. Clean up the code

## Update (09/05/2021)
1. More cleaning for noise in text,
1. Add `probablepeople` company name parser,
1. Memory now stores counts and probabilities,
1. Memory does not need to store all items like `'MARKS AND SPENCERS LTD', 'MS CORPORATION Limited'`, only the extracts,
1. Classification is now based on all tests:
   
   1. Each line of text gets scores for each possible class,
      
   1. Scores are weighted by a set of hardcoded values,
      
   1. Probability is taken from the memory and added to the score (weighted),
      
   1. Location score depends on the `importance` from the geocoder. "Coventry" gets a higher score than "Toys",
      
   1. Argmax to get the best class.
   
Formula for score:

$`\text{score} = \text{flag} \times \text{weight} + \text{flag}\times (1-\text{weight})\times \text{prob}`$

Possible improvements:
1. Spelling correction

### New output
```
===== Settings =====
Namespace(verbose=True)
======= End ========

Loaded an NLP pipeline...
Reading file: /home/tomasz/Documents/vectorai-coding-interview/data/lines.txt

=== Classifying ====
MARKS AND SPENCERS LTD             : Company Name        	 [1.0, 0.0, 0.0, 0.6, -1.0]
LONDON                             : Location            	 [0.0, 0.0, 0.0, 0.6, 1.9307827616237296]
ICNAO02312                         : Serial Number       	 [0.0, 0.0, 0.8, 0.0, 0.0]
LONDON GREAT BRITAIN               : Location            	 [0.0, 0.0, 0.0, 0.6, 2.0307827616237297]
TOYS                               : Good                	 [0.0, 0.0, 0.0, 0.6, 0.101]
INTEL LLC                          : Company Name        	 [1.0, 0.0, 0.0, 0.6799999999999999, -1.0]
MS CORPORATION Limited             : Company Name        	 [1.0, 0.0, 0.0, 0.6666666666666666, -1.0]
LONDON ENGLAND                     : Location            	 [0.0, 0.0, 0.0, 0.6571428571428571, 2.0307827616237297]
SLOUGH SE12 2XY                    : Address             	 [0.0, 1.0, 0.8250000000000001, 0.0, 0.0]
33 TIMBER YARD LONDON L1 8XY       : Address             	 [0.0, 1.0, 0.0, 0.6444444444444444, 1.0]
44 CHINA ROAD KOWLOON HONG KONG    : Address             	 [0.0, 1.0, 0.0, 0.64, 1.0]
44 CHINA ROAD HONG KONG            : Address             	 [0.0, 1.0, 0.0, 0.6363636363636364, 1.0]
XYZ 13423 / ILD                    : Serial Number       	 [0.0, 0.0, 0.8166666666666667, 0.0, 0.0]
ABC/ICL/20891NC                    : Serial Number       	 [0.0, 0.0, 0.8307692307692308, 0.0, 0.0]
HARDWOOD TABLE                     : Good                	 [0.0, 0.0, 0.0, 0.6285714285714286, 0.0]
PLASTIC BOTTLE                     : Good                	 [0.0, 0.0, 0.0, 0.6533333333333333, 0.27499999999999997]
PLASTIC BOTTLE                     : Good                	 [0.0, 0.0, 0.0, 0.675, 0.27499999999999997]
NVIDIA Ireland                     : Company Name        	 [1.0, 0.0, 0.0, 0.6941176470588235, -1.0]
NVIDIA Corporation                 : Company Name        	 [1.0, 0.0, 0.0, 0.6888888888888889, -0.889]
ICNAO02312                         : Serial Number       	 [0.0, 0.0, 0.8315789473684211, 0.0, 0.0]
ICN   AO02312                      : Serial Number       	 [0.0, 0.0, 0.8400000000000001, 0.0, 0.0]
ICNA O02312                        : Serial Number       	 [0.0, 0.0, 0.8476190476190476, 0.0, 0.0]
Asia                               : Location            	 [0.0, 0.0, 0.0, 0.6727272727272727, 0.8750525830245206]
Warsaw Poland                      : Location            	 [0.0, 0.0, 0.0, 0.6695652173913044, 1.7366192757766008]
Coventry                           : Location            	 [0.0, 0.0, 0.0, 0.6666666666666666, 0.7027519006502161]
PLASTIC BOTTLES                    : Good                	 [0.0, 0.0, 0.0, 0.6639999999999999, 0.27499999999999997]
Coventry UK                        : Location            	 [0.0, 0.0, 0.0, 0.676923076923077, 1.702751900650216]
======= End ========

== Memory content ==
Company Names: 	 count: 5,	 prob: 0.18518518518518517
	Group 0	 group extract: ms                                 	 Count: 2

	Group 1	 group extract: intel                              	 Count: 1

	Group 2	 group extract: nvidia                             	 Count: 2

=============
Company Addresses: 	 count: 4,	 prob: 0.14814814814814814
	Group 0	 group extract: slough se12 2xy                    	 Count: 1

	Group 1	 group extract: 33 timber yard london l1 8xy       	 Count: 1

	Group 2	 group extract: 44 china road kowloon hong kong    	 Count: 2

=============
Serial Nums: 	 count: 6,	 prob: 0.2222222222222222
	Group 0	 group extract: icnao02312                         	 Count: 4

	Group 1	 group extract: xyz13423/ild                       	 Count: 1

	Group 2	 group extract: abc/icl/20891nc                    	 Count: 1

=============
Goods: 	 count: 5,	 prob: 0.18518518518518517
	Group 0	 group extract: toy                                	 Count: 1

	Group 1	 group extract: hardwood table                     	 Count: 1

	Group 2	 group extract: plastic bottle                     	 Count: 3

=============
Locations: 	 count: 7,	 prob: 0.25925925925925924
	Group 0	 group extract: London, Greater London, England, United Kingdom	 Count: 3

	Group 1	 group extract: Asia                               	 Count: 1

	Group 2	 group extract: Warszawa, województwo mazowieckie, Polska	 Count: 1

	Group 3	 group extract: Coventry, West Midlands Combined Authority, West Midlands, England, United Kingdom	 Count: 2

=============
```

**Possible improvements:**
1. Geocode "goods" because sometimes locations get misclassified. It would also not be ideal as many "goods"
    are also locations.
1. Similarity score
1. More address RegEx patterns

## Classification
My approach for classification of a line of text was:
1. Check if it is an address. If true continue.
1. Get a list of named entities in the line using spaCy
1. If the list of NEs contains "ORG" classify as an organisation; continue
1. If the list of NEs contains "GPE" classify as location; continue
1. If all checks above fail, then it means we have a "good" or a "serial number". All goods should be in an English dictionary. Use this fact to classify.

I have arranged the checks in order of their precision. It is relatively easy to classify addresses using RegEx. After the classification, I do not run other checks on the line. It is based on the assumption that a line can only have one label.

## Clustering
### Company Names
Company names are similar if first words of their clean (lowercase), collapsed ([name] and [name] to [n]&[n]), trimmed (no "llc", "corporation"...) forms are the same.
### Addresses
Addresses are similar if they consist of the same set of words.
### Serial Numbers
Serial numbers are similar if their clean (no whitespaces, lowercase) forms are.
### Goods
Goods are similar if their lemmatized forms are.
### Locations
Locations are similar if their geocodings are.

## How to run
1. Copy a conda environment.
   ```shell
   conda env create -f environment.yml
   conda activate vectorai
   python -m spacy download en_core_web_trf
   pip install probablepeople
   ```
   I have created in on Linux and it is problematic to export conda envs between different 
   OS-s. I only use a few libraries so you might not need to clone my conda env.
   ```
     - python=3.8
     - spacy
     - geopy
     - pip:
       - pyenchant
   ```
1. Activate the environment (if using) 
2. Run ```main.py```
   ```shell
   python3 main.py
   ```

## Example output
```
===== Settings =====
Namespace(verbose=True)
======= End ========

Reading file: /home/tomasz/Documents/vectorai-coding-interview/data/lines.txt

=== Classifying ====
======= End ========

[...]

== Memory content ==
Company Names
	Group 0	 group extract: m&s
		Items: ['MARKS AND SPENCERS LTD', 'M&S CORPORATION Limited']

	Group 1	 group extract: intel
		Items: ['INTEL LLC']

	Group 2	 group extract: nvidia
		Items: ['NVIDIA Ireland', 'NVIDIA Corporation']

=============
Company Addresses
	Group 0	 group extract: slough se12 2xy
		Items: ['SLOUGH SE12 2XY']

	Group 1	 group extract: 33 timber yard london l1 8xy
		Items: ['33 TIMBER YARD LONDON L1 8XY']

	Group 2	 group extract: 44 china road kowloon hong kong
		Items: ['44 CHINA ROAD KOWLOON HONG KONG', '44 CHINA ROAD HONG KONG']

=============
Serial Nums
	Group 0	 group extract: icnao02312
		Items: ['ICNAO02312', 'ICNAO02312', 'ICN   AO02312', 'ICNA O02312']

	Group 1	 group extract: xyz13423/ild
		Items: ['XYZ 13423 / ILD']

	Group 2	 group extract: abc/icl/20891nc
		Items: ['ABC/ICL/20891NC']

=============
Goods
	Group 0	 group extract: toy
		Items: ['TOYS']

	Group 1	 group extract: hardwood table
		Items: ['HARDWOOD TABLE']

	Group 2	 group extract: plastic bottle
		Items: ['PLASTIC BOTTLE', 'PLASTIC BOTTLES']

	Group 3	 group extract: asia
		Items: ['Asia']

	Group 4	 group extract: coventry
		Items: ['Coventry']

=============
Locations
	Group 0	 group extract: London, Greater London, England, United Kingdom
		Items: ['LONDON', 'LONDON GREAT BRITAIN', 'LONDON ENGLAND']

	Group 1	 group extract: Warszawa, województwo mazowieckie, Polska
		Items: ['Warsaw Poland']

=============
```

## Help
```
usage: main.py [-h] [-v VERBOSE]

VectorAI coding interview.

optional arguments:
  -h, --help            show this help message and exit
  -v VERBOSE, --verbose VERBOSE
                        Print messages.
```

## Libraries used
1. https://spacy.io/
1. https://pyenchant.github.io/pyenchant/
1. https://geopy.readthedocs.io/en/stable/
