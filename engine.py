import os
import re
from collections import Counter

# spellchecker for goods vs IDs
import enchant
import numpy as np
# for all nlp
import spacy

from memory import Memory


class Engine:
    def __init__(self, file_path, verbose=True):

        self.verbose = verbose

        self.lines = []
        self.file_path = file_path

        # how confident are we in the expert (rule based classifications)
        # 0 for does not work at all, 1 for always works
        # can be used to adjust misclassifications
        self.expert_strengths = {
            "Company Name": 1,
            "Address": 1,
            "Serial Number": .8,
            "Good": .6,
            "Location": 1
        }
        self.expert_strengths_list = list(self.expert_strengths.values())
        self.lab_list = ["Company Name", "Address", "Serial Number", "Good", "Location"]
        self.current_item_scores = {
            "Company Name": 0,
            "Address": 0,
            "Serial Number": 0,
            "Good": 0,
            "Location": 0
        }
        # self.nlp = spacy.load("en_core_web_sm")

        # install a pipeline if it does not exist
        try:
            self.nlp = spacy.load("en_core_web_trf")
            print("Loaded an NLP pipeline...")
        except:
            print("Downloading an NLP pipeline...")
            os.system("python -m spacy download en_core_web_trf")
            self.nlp = spacy.load("en_core_web_trf")
            print("Loaded an NLP pipeline...")

        self.memory = Memory(self.nlp)

        # self.matcher = Matcher(self.nlp.vocab)

        # might be necessary to use more dicts for more accurate results
        self.spell_check = enchant.Dict("en_GB")

        # number name
        # we can add more patterns
        # for example street format [street] [number]
        # or non-UK postcodes
        self.regex_street_1 = "^\d+\s[A-z]+\s[A-z]+"
        # city post code UK https://en.wikipedia.org/wiki/Postcodes_in_the_United_Kingdom#Validation
        self.regex_uk_post_code = "([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9][A-Za-z]?))))\s?[0-9][A-Za-z]{2})"
        # check for location

        # self.location_pattern_spacy = [[{"ENT_TYPE": "GPE"}]]
        # self.comp_name_pattern_spacy = [[{"ENT_TYPE": "ORG"}]]
        #
        # self.matcher.add("location", self.location_pattern_spacy)
        # self.matcher.add("comp_name", self.comp_name_pattern_spacy)

    def read_from_file(self):
        """
        Read lines from a text file.
        """
        if self.verbose:
            print(f"Reading file: {self.file_path}")
        with open(self.file_path) as file_in:
            for line in file_in:
                # strip spaces, ""-s, ,-s
                self.lines.append(line.strip().replace('"', '').replace(',', ''))

    def _regex_match(self, text: str) -> bool:
        """
        Match text with RegEx patterns.
        :param text: input text
        :return: bool whether match or not
        """
        x1 = re.search(self.regex_street_1, text)
        x2 = re.search(self.regex_uk_post_code, text)
        if x2 or x1:
            # we have an address
            return True
        return False

    # def match_spacy_rules(self, doc):
    #     matches = self.matcher(doc)
    #     for match_id, start, end in matches:
    #         print(self.nlp.vocab.strings[match_id])

    def clean_lines(self, text_l: list) -> list:
        """
        Remove some special characters from text and strip.
        :param text_l: list of lines of text (strings)
        :return: list of processed lines of text
        """
        text_l_out = []
        for text in text_l:
            text = text.strip()
            text = re.sub('[!,*)@#%(&$_?.^]', '', text)
            text_l_out.append(text)

        return text_l_out

    def classify(self, lines: list, add_to_list=False) -> list:
        """
        Classify each line in a list of lines.
        :param lines: list of lines
        :param add_to_list: add to memory or only classify
        :return: list of labels
        """
        if self.verbose:
            print()
            print(" Classifying ".center(20, "="))
        lines = self.clean_lines(lines)
        outs = []
        for doc in self.nlp.pipe(lines):
            # ["Company Name", "Address", "Serial Number", "Good", "Location"]
            flags = [0, 0, 0, 0, 0]
            # check for addresses
            if self._regex_match(doc.text):
                flags[1] = 1
                if add_to_list:
                    self.memory.append_(doc.text, 1)
                    continue

            geocode = self.memory.geolocator.geocode(doc.text)
            # get NER labels for the line
            labels = [ent.label_ for ent in doc.ents]

            # check for comp names
            if "ORG" in labels:
                # if contains an ORG tag
                flags[0] = 1
                flags[4] -= 1
                if add_to_list:
                    self.memory.append_(doc.text, 0)
                    continue

            # print(geocode)
            # check for locations
            if "GPE" in labels:
                # if contains an GPE tag
                flags[4] = 1
                if add_to_list:
                    self.memory.append_(doc.text, 4)
                    continue

            if geocode:
                flags[4] += geocode.raw['importance']

            # check for goods
            # spaCy says everything is in vocab, not useful
            # for token in doc:
            #     print(token.is_oov)
            #     print(token.orth in self.nlp.vocab)

            # if most words are in the English Dict then we have a good else an ID
            lang_labels = [self.spell_check.check(token.text) for token in doc]
            # get mode of a list
            mode_lang_label = Counter(lang_labels).most_common(1)[0][0] if lang_labels else None
            if mode_lang_label:
                # is a good
                flags[3] = 1
                if add_to_list:
                    self.memory.append_(doc.text, 3)
            else:
                # has to be a serial number/ID
                flags[2] = 1
                if add_to_list:
                    self.memory.append_(doc.text, 2)

            # calculate score
            scores = self._calculate_score(flags)
            best_class = np.argmax(scores)
            if self.verbose:
                print(f"{str(doc.text).ljust(35)}: {self.lab_list[best_class].ljust(20)}\t {scores}")
            # append to the memory
            self.memory.append_(doc.text, best_class)
            outs.append(best_class)

        if self.verbose:
            print(" End ".center(20, "="))
            print()
        return outs

    def _calculate_score(self, flags: list) -> list:
        """
        Calculate a weighted classification scores using flags, weights, and probabilities
        :param flags: classification flags
        :return: list of scores for each group
        """
        scores = [0, 0, 0, 0, 0]
        for i in range(len(flags)):
            scores[i] = flags[i]*self.expert_strengths_list[i] + flags[i]*(1-self.expert_strengths_list[i])*self.memory.get_probs()[i]
        return scores

    def print_class(self, lines: list, labels: list):
        """
        Pretty print lines and their labels
        :param lines: list of lines
        :param labels: list of labels
        """
        for line, label in zip(lines, labels):
            print(f"Text: {line}")
            print(f"label: {label}, \t text label: {self.lab_list[label]}")
            print()

    def test(self):
        for line in self.lines:
            doc = self.nlp(line)
            print(f"Text: {doc}")
            print(f"Named entities:")
            for ent in doc.ents:
                print(f"ent: {ent}, label: {ent.label_}, start: {ent.start_char}, end: {ent.end_char}")
            print("======")

    def _print(self):
        print("Company Names")
        print(self.comp_names)
        print("=============")
        print("Company Addresses")
        print(self.comp_adds)
        print("=============")
        print("Serial Nums")
        print(self.serial_nums)
        print("=============")
        print("Goods")
        print(self.goods)
        print("=============")
        print("Locations")
        print(self.locations)
        print("=============")

    def run(self):
        self.read_from_file()
        # self.test()
        labels = self.classify(self.lines)

        if self.verbose:
            # self.print_class(self.lines, labels)
            self.memory.print()
