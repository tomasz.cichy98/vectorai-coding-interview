# $ pip install .

from setuptools import find_packages, setup

setup(
    name='Vector.ai coding interview',
    packages=find_packages(),
    version='0.1',
    description='Vector.ai coding interview',
    author='Tomasz Cichy',
    license='MIT'
)
