import argparse
import os
import re

from classifier import Classifier
from engine import Engine


def str2bool(v):
    """
    Allow True/False for argparse.
    """
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def clean_lines(text_l: list) -> list:
    """
    Remove some special characters from text and strip.
    :param text_l: list of lines of text (strings)
    :return: list of processed lines of text
    """
    text_l_out = []
    for text in text_l:
        text = text.strip()
        text = re.sub('[!,*)@#%(&$_?.^]', '', text)
        text_l_out.append(text)

    return text_l_out


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='VectorAI coding interview.')
    parser.add_argument("-v", "--verbose", dest="verbose", help="Print messages.",
                        default=True, type=str2bool)
    parser.add_argument("-b", "--use-bert", dest="use_bert", help="Use BERT for classification.",
                        default=True, type=str2bool)

    args = parser.parse_args()
    if args.verbose:
        print(" Settings ".center(20, "="))
        print(args)
        print(" End ".center(20, "="))
        print()

    path = os.path.dirname(os.path.realpath(__file__)) + "/data/lines.txt"
    if not args.use_bert:
        eng = Engine(path, verbose=args.verbose)
        eng.run()
    else:
        # make a classifier
        cls = Classifier()
        cls.load_weights(bonus_name="epochs0")
        lines = []
        # read from file
        with open(path) as f:
            for line in f:
                # strip spaces, ""-s, ,-s
                lines.append(line.strip().replace('"', '').replace(',', ''))
        # clean lines
        lines = clean_lines(lines)
        # classify
        for line in lines:
            cls.fit_one(line)
        # print
        cls.memory.print()
