import re
import string
import spacy

from geopy.geocoders import Nominatim
import probablepeople as pp


class Memory:
    def __init__(self, nlp, keep_items=True):
        if nlp is None:
            self.nlp = spacy.load("en_core_web_trf")
        else:
            self.nlp = nlp

        self.lab_list = ["Company Name", "Address", "Serial Number", "Good", "Location"]
        self.counter = [0, 0, 0, 0, 0]
        self.probs = [0, 0, 0, 0, 0]
        self.n = 0
        self.comp_names = {}  # label: 0
        self.comp_adds = {}  # label: 1
        self.serial_nums = {}  # label: 2
        self.goods = {}  # label: 3
        # country, continent
        self.locations = {}  # label: 4
        self.keep_items = keep_items

        self.pattern = "\w+\s(and)\s\w+"

        # might be problematic because of api request limits
        # it is otherwise free
        self.geolocator = Nominatim(user_agent="vector_coding_interv")

    def _store_item(self, l_in, item, item_temp):
        if self.keep_items:
            l_in[item_temp].append(item)
        else:
            l_in[item_temp] += 1

    def _add_new_group(self, l_in, item, item_temp):
        if self.keep_items:
            l_in[item_temp] = [item]
        else:
            l_in[item_temp] = 1

    def _collapse_comp_name(self, text: str) -> str:
        """
        Convert [name] and [name] to [n]&[n]
        :param text: input text
        :return: converted text
        """
        # Mark and Spancer -> M&S and others
        out = text
        x = re.search(self.pattern, text)
        if x:
            parts = text.split()[:3]
            out = f"{parts[0][0]}{parts[2][0]}"
        return out

    def _process_comp_name(self, text: str) -> str:
        """
        Clean a company name. For the company name cluster.
        to lower
        collapse name
        remove words like llc, limited etc
        :param text: input text
        :return: clean text
        """
        out = text.lower()
        out = self._collapse_comp_name(out)
        # out = out.replace("llc", "").replace("limited", "").replace("company", "").replace("corporation", "").replace(
        #     "the", "")
        # # keep only first word
        # out = out.split()[0]
        out = pp.parse(out)[0][0]
        return out

    def _append_name(self, item: str):
        """
        Cluster company name. If a processed name already exists in a cluster, add to this cluster,
        if not, make a new cluster.
        :param item: input text
        :return: None
        """
        item_temp = self._process_comp_name(item)
        for el in self.comp_names:
            # match with the key
            if el == item_temp:
                self._store_item(l_in=self.comp_names, item=item, item_temp=item_temp)
                return
        # make new group
        self._add_new_group(l_in=self.comp_names, item=item, item_temp=item_temp)

    def _append_address(self, item: str):
        """
        Cluster addresses. If there already exists a cluster with address with the exact same words
        (might be in different order) add to this cluster.
        :param item: input text
        :return: None
        """
        item_temp = item.lower()
        for el in self.comp_adds:
            x = all(e in item_temp.split() for e in el.split())
            y = all(e in el.split() for e in item_temp.split())
            if x or y:
                if self.keep_items:
                    self.comp_adds[el].append(item)
                else:
                    self.comp_adds[el] += 1
                return
        # make new group
        self._add_new_group(self.comp_adds, item, item_temp)
        # self.comp_adds[item_temp] = [item]

    def _clean_string(self, text: str) -> str:
        """
        Clean serial number string. Remove whitespaces and to lower
        :param text: input text
        :return: clean string
        """
        return text.translate({ord(text): None for text in string.whitespace}).lower()

    def _append_serial(self, item: str):
        """
        Serial number it either identical or different
        :param item: input text
        """
        # clean string
        item_temp = self._clean_string(item)
        # check all groups
        for el in self.serial_nums:
            if el == item_temp:
                self._store_item(self.serial_nums, item, item_temp)
                return
        # make new group
        # we can change the sub-list to a set() so there are no duplicates
        self._add_new_group(self.serial_nums, item, item_temp)

    def _lemmatize(self, text: str) -> str:
        """
        Convert a string to its lemmatized form.
        :param text: input text
        :return: lemmatized text
        """
        lem = []
        for token in self.nlp(text):
            lem.append(token.lemma_)
        # list to string
        lem_str = " ".join(lem)
        lem_str = lem_str.lower()
        return lem_str

    def _append_good(self, item: str):
        """
        Lemmatize and compare
        :param item: input text
        """
        item_temp = self._lemmatize(item)
        for el in self.goods:
            if el == item_temp:
                self._store_item(self.goods, item, item_temp)
                return
        self._add_new_group(self.goods, item, item_temp)

    def _append_location(self, item: str):
        """
        If a geocoded location of an element is the same as of an item in a cluster, add to the cluster.
        :param item: input text
        :return: None
        """
        location = self.geolocator.geocode(item).address
        # check all groups
        for el in self.locations:
            if el == location:
                self._store_item(self.locations, item, location)
                # self.locations[el].append(item)
                return
        self._add_new_group(self.locations, item, location)
        # self.locations[location] = [item]

    def get_probs(self):
        div = self.n if self.n != 0 else 1e-10
        self.probs = [c/div for c in self.counter]
        return self.probs

    def append_(self, item: str, label: int):
        """
        Process how to append an element to the memory. Select correct destination.
        :param item: input text
        :param label: predicted label
        """
        self.n += 1
        self.counter[label] += 1
        if label == 0:
            self._append_name(item)
        elif label == 1:
            self._append_address(item)
        elif label == 2:
            self._append_serial(item)
        elif label == 3:
            self._append_good(item)
        elif label == 4:
            self._append_location(item)
        else:
            raise ValueError("Wrong label.")

    def _print_groups(self, l_in: dict):
        """
        Pretty print clusters
        :param l_in: dict of lists
        """
        for i, l in enumerate(l_in):
            print(f"\tGroup {i}\t group extract: {l.ljust(35)}", end="")
            if not self.keep_items:
                print(f"\t Count: {l_in[l]}")
            else:
                print()
                print(f"\t\tItems: {l_in[l]}")
            print()

    def print(self):
        self.get_probs()
        print(" Memory content ".center(20, "="))
        print(f"Company Names: \t count: {self.counter[0]},\t prob: {self.probs[0]}")
        self._print_groups(self.comp_names)
        print("=============")
        print(f"Company Addresses: \t count: {self.counter[1]},\t prob: {self.probs[1]}")
        self._print_groups(self.comp_adds)
        print("=============")
        print(f"Serial Nums: \t count: {self.counter[2]},\t prob: {self.probs[2]}")
        self._print_groups(self.serial_nums)
        print("=============")
        print(f"Goods: \t count: {self.counter[3]},\t prob: {self.probs[3]}")
        self._print_groups(self.goods)
        print("=============")
        print(f"Locations: \t count: {self.counter[4]},\t prob: {self.probs[4]}")
        self._print_groups(self.locations)
        print("=============")
