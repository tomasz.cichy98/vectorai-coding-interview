import random
import string

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from torch.utils.data import Dataset
from tqdm import tqdm
from transformers import BertTokenizer


class VectorDataset(Dataset):
    def __init__(self, data, max_len=57):
        self.data_whole = data
        self.labels = self.data_whole["label"].tolist()
        self.labels_num = []
        self._encode_labels()
        self.texts = self.data_whole["text"].tolist()
        self.input_ids = []
        self.attention_masks = []

        self.MAX_LEN = max_len + 1
        print(f"\tText max len: {self.MAX_LEN}")
        self.PRE_TRAINED_MODEL_NAME = 'bert-base-cased'
        self.tokenizer = BertTokenizer.from_pretrained(self.PRE_TRAINED_MODEL_NAME)

        self.bertify_all()

    def _encode_labels(self):
        lab_list = ["company", "address", "serial", "good", "location"]
        for label in self.labels:
            self.labels_num.append(lab_list.index(label))

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, item):
        return self.texts[item], self.input_ids[item], self.attention_masks[item], self.labels_num[item]

    def augment(self):
        pass

    def bertify_all(self):
        """
        Create embeddings for training using BERT tokenizer.
        """
        print("\tBERT encoding...")
        for text in self.texts:
            encoding = self.tokenizer.encode_plus(
                text,
                add_special_tokens=True,
                return_token_type_ids=False,
                padding="max_length",
                max_length=self.MAX_LEN,
                return_attention_mask=True,
                return_tensors="pt"
            )
            # store
            self.input_ids.append(encoding["input_ids"].flatten())
            self.attention_masks.append(encoding['attention_mask'].flatten())
        print("\tDone encoding.")


def process_addresses():
    """
    Process the addresses csv.
    :return: pandas dataframe
    """
    print("Processing addresses.")
    addresses = []
    cols = ["NUMBER", "STREET", "CITY"]
    path1 = "./data/poland.csv"
    path2 = "./data/sweden.csv"
    batch = int(6_500 / 2)
    # read files
    # get lines to skip
    total_size = sum(1 for line in open(path1))
    skip = sorted(random.sample(range(1, total_size + 1), total_size - batch))

    df_pl = pd.read_csv(path1, usecols=cols, skiprows=skip)

    total_size = sum(1 for line in open(path2))
    skip = sorted(random.sample(range(1, total_size), total_size - batch))

    df_sw = pd.read_csv(path2, usecols=cols, skiprows=skip)

    # combine
    df_whole = pd.concat([df_pl, df_sw]).dropna()
    # make address
    for _, row in tqdm(df_whole.iterrows()):
        line = f"{row['NUMBER']} {row['STREET']}, {row['CITY']}"
        addresses.append(line)
    # make df
    labels = ["address"] * len(addresses)
    d = {"text": addresses, "label": labels}
    return pd.DataFrame(d)


def process_comp_names():
    """
    Process the company name csv.
    :return: pandas dataframe
    """
    print("Processing company names.")
    path = "./data/AllCompanyNames.csv"

    batch = 5_000
    total_size = 3_838_469
    # total_size = sum(1 for line in open(path))
    skip = sorted(random.sample(range(1, total_size + 1), total_size - batch))

    df = pd.read_csv(path, usecols=["CompanyName"], skiprows=skip)
    labels = ["company"] * len(df)
    df["label"] = labels
    df = df.rename(columns={"CompanyName": "text", "label": "label"})
    return df


def process_locations():
    """
    Process the location csv.
    :return: pandas dataframe
    """
    locations = []
    path = "./data/worldcities.csv"
    columns = ["city", "country"]
    batch = 5_000
    total_size = sum(1 for line in open(path))
    skip = sorted(random.sample(range(1, total_size + 1), total_size - batch))
    df = pd.read_csv(path, usecols=columns, skiprows=skip)

    for _, row in df.iterrows():
        line = f"{row['city']}, {row['country']}"
        locations.append(line)

    labels = ["location"] * len(locations)
    d = {"text": locations, "label": labels}
    return pd.DataFrame(d)


def generate_serials(how_many, min_len=5, max_len=20):
    """
    Generate serial numbers.
    :return: pandas dataframe
    """
    # get random len
    serials = []
    labels = ["serial"] * how_many
    for _ in range(how_many):
        n = np.random.randint(min_len, max_len)
        serial = ''.join(random.choices(string.ascii_uppercase + string.digits + string.punctuation, k=n))
        serials.append(serial)
    d = {"text": serials, "label": labels}
    df_out = pd.DataFrame(d)
    return df_out


def show_class_volumes(df):
    """
    Summarise the dataframe
    :param df: pandas dataframe
    :return: summary
    """
    summary = df.groupby("label").agg('count')
    print(summary)
    return summary


if __name__ == "__main__":
    # process files
    add_df = process_addresses()
    com_df = process_comp_names()
    loc_df = process_locations()
    raw_df = pd.read_csv("./data/vector_dataset.csv")

    # make serials
    serials = generate_serials(6000)
    # combine
    raw_df = pd.concat([raw_df, serials, add_df, com_df, loc_df])

    # summarise
    show_class_volumes(raw_df)

    max_len = len(max(raw_df["text"].tolist(), key=len))
    # train test split
    train_df, test_df = train_test_split(raw_df)
    val_df, test_df = train_test_split(test_df, test_size=0.5)
    # save to file
    train_df.to_csv("./data/train.csv")
    test_df.to_csv("./data/test.csv")
    val_df.to_csv("./data/val.csv")

    # sanity check
    train_set = VectorDataset(train_df, max_len=max_len)
    test_set = VectorDataset(test_df, max_len=max_len)
    val_set = VectorDataset(val_df, max_len=max_len)

    print(len(train_set))
    print(train_set.__getitem__(100))
    print(test_set.__getitem__(100))
    print(val_set.__getitem__(100))
